<h1 align='center'> 码上致富 盲盒项目 </h1>

## 项目介绍

最近比较火的盲盒系统，该项目是基于uniapp开发的盲盒项目，有需要的朋友可以联系我，运营级的项目，本次开源的是uniapp前端模板，选用技术为JAVA，采用框架：spring boot+mybatis+vue开发，盲盒APP大致如下：

![Image text](https://gitee.com/mashangzhifu/mszf-mh/raw/master/desc.png)

H5演示地址：

```
http://39.105.97.89:8099/mh/static/#/
```

账号：15116454377

密码：11111111

演示系统加载图片可能有点慢，演示系统开盒的时候用余额支付就行了，里面有余额，打包成APP，即可直接支付。


新增APP演示：

 <img src="https://gitee.com/mashangzhifu/mszf-mh/raw/master/download_qr.png" width = "200" height = "200" alt="盲盒APP演示地址" align=center />


## 产品展示

![Image text](https://gitee.com/mashangzhifu/mszf-mh/raw/master/ui.png)

相关界面：

![Image text](https://gitee.com/mashangzhifu/mszf-mh/raw/master/0.png)

![Image text](https://gitee.com/mashangzhifu/mszf-mh/raw/master/1.png)
![Image text](https://gitee.com/mashangzhifu/mszf-mh/raw/master/2.png)
![Image text](https://gitee.com/mashangzhifu/mszf-mh/raw/master/3.png)
![Image text](https://gitee.com/mashangzhifu/mszf-mh/raw/master/4.png)
![Image text](https://gitee.com/mashangzhifu/mszf-mh/raw/master/5.png)
![Image text](https://gitee.com/mashangzhifu/mszf-mh/raw/master/6.png)
![Image text](https://gitee.com/mashangzhifu/mszf-mh/raw/master/7.png)
![Image text](https://gitee.com/mashangzhifu/mszf-mh/raw/master/8.png)
![Image text](https://gitee.com/mashangzhifu/mszf-mh/raw/master/9.png)

## 源码运行

下载下来，之后，通过node安装如下依赖，即可查看效果：

```bash
  npm install uni-simple-router #  或者： yarn add uni-simple-router
```

```
npm install uni-simple-router
```

```
npm install uni-read-pages
```

```
npm install clipboard --save
```



公众号：

![Image text](https://gitee.com/mashangzhifu/mszf-mh/raw/master/qrcode.jpg)

客服微信：

![Image text](https://gitee.com/mashangzhifu/mszf-mh/raw/master/wx.jpg)
