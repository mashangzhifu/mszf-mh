import Vue from 'vue'
import App from './App'
import uView from "uview-ui";

import utils from './common/utils'

// 全局存储 vuex 的封装
import store from '@/store';
import {router,RouterMount} from '@/common/router.js'  //路径换成自己的
Vue.use(router);

Vue.use(uView);


// 引入 uView 提供的对 vuex 的简写法文件
let vuexStore = require('@/store/$u.mixin.js');
Vue.mixin(vuexStore);

Vue.config.productionTip = false



Vue.prototype.$utils = utils;

App.mpType = 'app'

const app = new Vue({
	store,
    ...App
})


// http 拦截器，将此部分放在 new Vue() 和 app.$mount() 之间，才能 App.vue 中正常使用
import httpInterceptor from '@/common/http.interceptor.js';
Vue.use(httpInterceptor, app);

// http 接口 API 抽离，免于写 url 或者一些固定的参数
import httpApi from '@/common/http.api.js';
Vue.use(httpApi, app);

//v1.3.5起 H5端 你应该去除原有的app.$mount();使用路由自带的渲染方式
// #ifdef H5
	RouterMount(app,router,'#app')
// #endif

// #ifndef H5
	app.$mount(); //为了兼容小程序及app端必须这样写才有效果
// #endif