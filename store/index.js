import config from '@/common/config.js';
import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

let cacheData = {};

try{
	// 尝试获取本地是否存在cacheData变量，第一次启动APP时是不存在的
	cacheData = uni.getStorageSync('cacheData');
}catch(e){
	
}

// 需要永久存储，且下次APP启动需要取出的，在state中的变量名

//使用方法为：如果要修改vuex的state中的user.name变量为"史诗" => this.$u.vuex('user.name', '史诗')
//vm.$u.vuex('vuex_token', data.token);
//vm.$u.vuex('vuex_user', data.user);
// 如果要修改vuex的state的version变量为1.0.1 => this.$u.vuex('version', '1.0.1')
//获取值方式如下：
//vm.vuex_token
//uni.getStorageSync('cacheData')
//this.$store.state.xxx

let saveStateKeys = ['vuex_user', 'vuex_token'];

// 保存变量到本地存储中
const saveLifeData = function(key, value){
	// 判断变量名是否在需要存储的数组中
	if(saveStateKeys.indexOf(key) != -1) {
		// 获取本地存储的cacheData对象，将变量添加到对象中
		let tmp = uni.getStorageSync('cacheData');
		// 第一次打开APP，不存在cacheData变量，故放一个{}空对象
		tmp = tmp ? tmp : {};
		tmp[key] = value;
		// 执行这一步后，所有需要存储的变量，都挂载在本地的cacheData对象中
		uni.setStorageSync('cacheData', tmp);
	}
}
// 简化 vuex 操作，文档：https://uviewui.com/components/vuexDetail.html
const store = new Vuex.Store({
	state: {
		// 如果上面从本地获取的cacheData对象下有对应的属性，就赋值给state中对应的变量
		// 加上vuex_前缀，是防止变量名冲突，也让人一目了然
		vuex_user: cacheData.vuex_user ? cacheData.vuex_user : {userName: '码上致富'},
		vuex_token: cacheData.vuex_token ? cacheData.vuex_token : '',
		cacheData: cacheData,
		// 如果vuex_version无需保存到本地永久存储，无需cacheData.vuex_version方式
		vuex_config: config,
	},
	mutations: {
		$uStore(state, payload) {
			// 判断是否多层级调用，state中为对象存在的情况，诸如user.info.score = 1
			let nameArr = payload.name.split('.');
			let saveKey = '';
			let len = nameArr.length;
			if(len >= 2) {
				let obj = state[nameArr[0]];
				for(let i = 1; i < len - 1; i ++) {
					obj = obj[nameArr[i]];
				}
				obj[nameArr[len - 1]] = payload.value;
				saveKey = nameArr[0];
			} else {
				// 单层级变量，在state就是一个普通变量的情况
				state[payload.name] = payload.value;
				saveKey = payload.name;
			}
			// 保存变量到本地，见顶部函数定义
			saveLifeData(saveKey, state[saveKey])
		}
	}
})

export default store
