import Clipboard from 'clipboard'
module.exports = {
	showLoading(title) {
		uni.showLoading({
			title: title
		});
	},
	showToast(title) {
		uni.showToast({
			title: title,
			mask: false,
			duration: 2000,
			icon: "none"
		});
	},
	// 空格截取
	trim : function(value) {
		if (value == null) {
			return "";
		}
		return value.toString().replace(/(^\s*)|(\s*$)|\r|\n/g, "");
	},
	// 判断字符串是否为空
	isEmpty : function(value) {
		if (value == null || this.trim(value) == "" || value== "undefined") {
			return true;
		}
		return false;
	},
	// 判断一个字符串是否为非空串
	isNotEmpty : function(value) {
		return !this.isEmpty(value);
	},
	handleClipboard (text, event, onSuccess, onError) {
	  event = event || {}
	  const clipboard = new Clipboard(event.target, {
	    text: () => text
	  })
	  clipboard.on('success', () => {
	    onSuccess()
	    clipboard.off('error')
	    clipboard.off('success')
	    clipboard.destroy()
	  })
	  clipboard.on('error', () => {
	    onError()
	    clipboard.off('error')
	    clipboard.off('success')
	    clipboard.destroy()
	  })
	  clipboard.onClick(event)
	},
	saveFile(url){
		var oA = document.createElement("a");
		oA.download = '';
		oA.href = url;
		document.body.appendChild(oA);
		oA.click();
		oA.remove(); // 下载之后把创建的元素删除
	},
	logout(){
		uni.removeStorageSync('cacheData');
		uni.removeStorageSync('vuex_user');
		uni.removeStorageSync('vuex_token');
	},
	formatTime (time) {
        let unixtime = time
        let unixTimestamp = new Date(unixtime * 1000)
        let Y = unixTimestamp.getFullYear()
        let M = ((unixTimestamp.getMonth() + 1) > 10 ? (unixTimestamp.getMonth() + 1) : '0' + (unixTimestamp.getMonth() + 1))
        let D = (unixTimestamp.getDate() > 10 ? unixTimestamp.getDate() : '0' + unixTimestamp.getDate())
        let toDay = Y + '-' + M + '-' + D
        return toDay
    },
	checkLogin(){

	}
}
	
