const config = {
	
	// 产品名称
	productName: '乐盒',
	
	// 公司名称
	companyName: '码上致富',
	
	// 产品版本号
	productVersion: 'V0.0.1',
	
	// 版本检查标识
	appCode: 'android',
	
	// 内部版本号码
	appVersion: 1,
	
	// 管理基础路径
	taokePath: '/taoke',
	
	apiPath: '/api',
	
	filterUrl:['api/login/check','api/login','login','mobile/login','auth/login','smsCode']
}

// 设置后台接口服务的基础地址
//config.baseUrl = 'http://39.105.97.89/dev-api';


// config.baseUrl = 'http://39.105.97.89/prod-api';
// config.appUrl = 'http://39.105.97.89:8083';
// config.tkUrl = 'http://39.105.97.89:8083/tk';

config.lesseeId='41';

// config.baseUrl = 'http://127.0.0.1:8088';
// config.appUrl = 'http://127.0.0.1:8088';
// config.tkUrl = 'http://127.0.0.1:8088/mh';


// config.baseUrl = 'http://manghe.vipgz4.idcfengye.com';
// config.appUrl = 'http://manghe.vipgz4.idcfengye.com';
// config.tkUrl = 'http://manghe.vipgz4.idcfengye.com/mh';

config.baseUrl = 'http://39.105.97.89:8088';
config.appUrl = 'http://39.105.97.89:8088';
config.tkUrl = 'http://39.105.97.89:8088/mh';

// 建议：打开下面注释，方便根据环境，自动设定服务地址
// if (process.env.NODE_ENV === 'development'){
// 	// config.baseUrl = '/../js'; // 代理模式 vue.config.js 中找到 devServer 设置的地址
// 	config.baseUrl = 'http://127.0.0.1:8980/js';
// }

export default config;