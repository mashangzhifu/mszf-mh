// 此处第二个参数vm，就是我们在页面使用的this，你可以通过vm获取vuex等操作
const install = (Vue, vm) => {
	
	// 参数配置对象
	const config = vm.vuex_config;
	
	// 将各个定义的接口名称，统一放进对象挂载到vm.$u.api(因为vm就是this，也即this.$u.api)下
	vm.$u.api = {
		//校验token是否有效
		check: (params = {}) => vm.$u.get(config.apiPath + '/login/check', params),
		//登陆注册短信验证码
		loginSmsCode: (params = {}) => vm.$u.post('login/smsCode', params),
		//忘记密码短信验证码
		forgetSmsCode: (params = {}) => vm.$u.post('forget/smsCode', params),
		//换绑手机号短信验证码
		changetieSmsCode: (params = {}) => vm.$u.post('changetie/smsCode', params),
		//更改密码
		forgetPwd: (params = {}) => vm.$u.post(config.apiPath +'/user/forget', params),
		//手机号登陆
		mobileLogin: (params = {}) => vm.$u.post('mobile/login', params),
		//短信验证码登陆
		smsCodeLogin: (params = {}) => vm.$u.post('sms/login', params),
		//退出登陆
		logout: (params = {}) => vm.$u.get('logout', params),
		//获取配置
		getConfig: (params = {}) => vm.$u.post(config.apiPath + '/config/get_config', params),
		//注册
		register: (params = {}) => vm.$u.post(config.apiPath + '/user/register', params),
		//banner
		banners: (params = {}) => vm.$u.post(config.apiPath  + '/banner/list', params),
		//分类列表
		classifys: (params = {}) => vm.$u.post(config.apiPath  + '/classify/list', params),
		//分类下的盒子
		classifyBoxs: (params = {}) => vm.$u.post(config.apiPath  + '/classify/box', params),
		//商品列表
		goods: (params = {}) => vm.$u.post(config.apiPath  + '/good/list', params),
		//发货申请商品详情
		shipGoodInfos: (params = {}) => vm.$u.post(config.apiPath  + '/good/shipGoodInfos', params),
		//最新开盒
		newestBoxs: (params = {}) => vm.$u.post(config.apiPath  + '/box/newest', params),
		//最新开盒当天数据
		newestDay: (params = {}) => vm.$u.post(config.apiPath  + '/box/open', params),
		//我的盒柜
		boxcabinets: (params = {}) => vm.$u.post(config.apiPath  + '/boxcabinet/list', params),
		//商品兑换列表
		exchanges: (params = {}) => vm.$u.post(config.apiPath  + '/exchange/list', params),
		//商品兑换
		convert: (params = {}) => vm.$u.post(config.apiPath  + '/exchange/convert', params),
		//盒子信息
		boxInfo: (params = {}) => vm.$u.post(config.apiPath  + '/box/info', params),
		//盒子商品
		boxGood: (params = {}) => vm.$u.post(config.apiPath  + '/box/goods', params),
		//盒子列表
		boxs: (params = {}) => vm.$u.post(config.apiPath  + '/box/list', params),
		//盒子开出的商品
		openGoods:(params = {}) => vm.$u.post(config.apiPath  + '/box/openGoods', params),
		//试一试
		test:(params = {}) => vm.$u.post(config.apiPath  + '/lottery/test', params),
		//开盒
		open:(params = {}) => vm.$u.post(config.apiPath  + '/lottery/open', params),
		//开盒奖品
		prize:(params = {}) => vm.$u.post(config.apiPath  + '/lottery/prize', params),
		//地址列表
		address:(params = {}) => vm.$u.post(config.apiPath+'/address/list', params),
		//新增地址
		addAddress:(params = {}) => vm.$u.post(config.apiPath+'/address/add', params),
		//地址信息
		addressInfo:(params = {}) => vm.$u.get(config.apiPath+'/address/info', params),
		//默认地址
		defAddress:(params = {}) => vm.$u.post(config.apiPath+'/address/defAddress', params),
		//优惠卷列表
		coupons:(params = {}) => vm.$u.get(config.apiPath+'/coupon/list', params),
		//领券
		grantCoupons:(params = {}) => vm.$u.post(config.apiPath  + '/coupon/grant', params),
		//订单列表
		orders:(params = {}) => vm.$u.post(config.apiPath+'/order/list', params),
		//物流信息
		logistics:(params = {}) => vm.$u.post(config.apiPath+'/order/logistics', params),
		//申请发货
		applyShip:(params = {}) => vm.$u.post(config.apiPath+'/order/applyShip', params),
		//积分列表
		integras:(params = {}) => vm.$u.post(config.apiPath+'/integra/list', params),
		//金币支付配置
		goldConfigs:(params = {}) => vm.$u.get(config.apiPath+'/goldConfig/list', params),
		//金币充值
		goldRecharge:(params = {}) => vm.$u.post(config.apiPath+'/recharge/goldRecharge', params),
		//商品回收
		goodRecycle:(params = {}) => vm.$u.post(config.apiPath+'/boxcabinet/recycle', params),
		//反馈内容
		feedback:(params = {}) => vm.$u.post(config.apiPath  + '/feedback/submit', params),
		//常见问题
		issues:(params = {}) => vm.$u.post(config.apiPath  + '/issue/list', params),
		//问题详情
		issueInfo:(params = {}) => vm.$u.post(config.apiPath  + '/issue/info', params),
		//签到
		signIn:(params = {}) => vm.$u.post(config.apiPath  + '/sign/in', params),
		//签到天数
		signCount:(params = {}) => vm.$u.post(config.apiPath  + '/sign/count', params),
		//签到信息
		signInfo:(params = {}) => vm.$u.post(config.apiPath  + '/sign/info', params),
		//用户相关
		user: {
			//更新用户昵称 
			updateNickName: (params = {}) => vm.$u.post(config.apiPath+'/user/update_nick_name', params),
			//换绑手机号
			changeTieMobile: (params = {}) => vm.$u.post(config.apiPath+'/user/change_tie_mobile', params),
			//绑定客户端ID
			bindDevice:(params = {}) => vm.$u.getText(config.apiPath+'/user/bindDevice', params),
			//用户信息
			info:(params = {}) => vm.$u.post(config.apiPath+'/user/info', params),
		},
	};
	
}

export default {
	install
}