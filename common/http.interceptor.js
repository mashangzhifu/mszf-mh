// 此处第二个参数vm，就是我们在页面使用的this，你可以通过vm获取vuex等操作
const install = (Vue, vm) => {
	
	// 通用请求头设定
	const sessionIdHeader = 'Authorization';
	
	// 请求参数默认配置
	Vue.prototype.$u.http.setConfig({
		baseUrl: vm.vuex_config.baseUrl,
		originalData: true, 
		header: {
			'content-type': 'application/json;charset=UTF-8'
		}
	});
	
	console.info('请求地址：' + vm.vuex_config.baseUrl);
	
	// 请求拦截，配置Token等参数
	Vue.prototype.$u.http.interceptor.request = (req) => {
		if (!req.header){
			req.header = [];
		}
		var path = req.url;
		//则包含该元素
		// if(vm.vuex_config.filterUrl.indexOf(path) == -1 && vm.vuex_token == ''){
		// 	uni.navigateTo({
		// 		url:'/pages/login/login'
		// 	})
		// 	return;
		// }
		// 设定传递 Token 认证参数
		if (!req.header[sessionIdHeader] && vm.vuex_token){
			req.header[sessionIdHeader] = vm.vuex_token;
		}
		//req.data.lesseeId=vm.vuex_config.lesseeId;
		//debugger
		// console.log('request', req);
		return req;
	}
	
	
	// 响应拦截，判断状态码是否通过
	Vue.prototype.$u.http.interceptor.response = async (res, req) => {
	//Vue.prototype.$u.http.interceptor.response = async (res, req) => {
		// console.log('response', res);
		let data = res.data;
		if (!(data)){
			vm.$u.toast('未连接到服务器')
			return false;
		}
		if (typeof data === 'object' && !(data instanceof Array)){
			if (data.token){
				vm.$u.vuex('vuex_token', data.token);
				if (data.user){
					//data.user.avatar = data.user.avatar ? data.user.avatar : '/static/img/logo.png'
					vm.$u.vuex('vuex_user', data.user);
				}
			}
			if (data.code == 401){
				vm.$u.vuex('vuex_token', '');
				uni.removeStorageSync('cacheData');
				uni.removeStorageSync('vuex_user');
			}
		}
		return data;
	}
	
	// 封装 get text 请求
	vm.$u.getText = (url, data = {}, header = {}) => {
		return vm.$u.http.request({
			dataType: 'text',
			method: 'GET',
			url,
			header,
			data
		})
	}
	
	// 封装 post json 请求
	vm.$u.postJson = (url, data = {}, header = {}) => {
		header['content-type'] = 'application/json';
		return vm.$u.http.request({
			url,
			method: 'POST',
			header,
			data
		})
	}
	
}

export default {
	install
}